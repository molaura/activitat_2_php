<?php
session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Comprovar divisors</title>
        <meta charset="UTF-8">
    </head>
    <body>
        <div style="width: 600px; margin:100px auto; border: 1px solid #666; padding:15px 10px;">

        <form name="formulari" method="post" action="comprovar_divisors.php">
            Introdueix un número: <input type="text" name="numDivisor" id="numDivisor"><br>
            <input type="submit" value="Resultat">
        </form>

        <?php
        if (!empty($_POST['numDivisor'])) {
            
            $num = $_POST['numDivisor'];
            $resultat = numPrimer($num);
           
            if ($resultat == true) {
                echo "$num Es primer";
            } else {
                echo "$num No es primer. Els seus divisors són: ";
                $guardarDivisors = array();

                for ($i = 1; $i <= $num; $i++) {
                    if ($num % $i != 0) {
                        continue;
                    }
                    $guardarDivisors[] = $i;
                }
                foreach ($guardarDivisors as $valor) {
                    print $valor . ' ';
                }
            }
        }
            function numPrimer($num) {
                $cont = 0;
                for ($i = 1; $i <= $num; $i++) {
                    if ($num % $i == 0) {
                        //echo “$i <br>”;
                        $cont++;
                    }
                }
                if ($cont == 2) {
                    return true;
                } else {
                    return false;
                }
            }
        
        
        ?>
            
        <br>
        <br>
        <a href="menu.php"><p>TORNA A LA PÀGINA INICIAL</p></a>
        </div>
        
        </body>
</html>

