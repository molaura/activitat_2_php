<?php

session_start();

$usuaris = array("pere" => "secret", "joan" => "rambo", "marta" => "ramba", "admin" => "admin");

$usuari = $_POST['user'];
$contrassenya = $_POST['password'];
$CORRECTE = isset($_POST['user']) && isset($_POST['password']) && isset($usuaris[$usuari]) && $usuaris[$usuari] == $contrassenya;

if ($CORRECTE) {
    $_SESSION['Usuari'] = $usuari;
    header('Location: menu.php');
} else {
    setcookie("errors", "No tens accés a la pàgina");
    header('Location: formulari_login.php');
}


