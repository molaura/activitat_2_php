<?php
session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Conjetura Collatz</title>
        <meta charset="UTF-8">
    </head>
    <body>
        <div style="width: 600px; margin:100px auto; border: 1px solid #666; padding:15px 10px;">

        <form name="formulari" method="post" action="conjetura_collatz.php">
            Introdueix un número: <input type="text" name="collatz" id="collatz"><br>
            <input type="submit" value="Resultat">
        </form>

        <?php
        if (!empty($_POST['collatz'])) {
            
            $num = $_POST['collatz'];
            $count = 0;
          do {
                if ($num % 2 == 0) {
                    $num = $num / 2;
                    echo $num . " ";
                } else {
                    $num = (3 * $num + 1);       
                    echo $num . " ";
                }
                $count ++;
            } while ($num > 1);
            echo "La iteració s'ha repetit $count vagedes";
        }
        ?>
            <br>
             <a href="menu.php"><p>TORNA A LA PÀGINA INICIAL</p></a>
        </div>
    </body>
</html>
